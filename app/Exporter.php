<?php

namespace App;

use App\Illuminate\StorageParams;

class Exporter
{
    protected StorageParams $storageParams;

    private array $final;

    public function __construct(StorageParams $storageParams)
    {
        $this->storageParams = $storageParams;
    }

    public function export() : array
    {
        $count = 0;
        $rowCount = 0;

        $this->final = [];

        $stepAverageArray = array();
        $startItem = array();

        $depths = [];

        $expeditionNumberFilter = $this->storageParams->params->get('expedition_number');
        $speedFilter = $this->storageParams->params->get('speed');

        $all = $this->storageParams->data->filter(fn(\stdClass $item) =>
            ($expeditionNumberFilter && $expeditionNumberFilter === $item->expedition_number) &&
            ($speedFilter && $item->speed >= $speedFilter)
        );

        foreach ($all as $item) {
            /*
             * $data
             * +"id": "d19274b8-474a-4057-8d46-37ec9fd7ebc0"
             * +"step_id": 70
             * +"latitude": 54.96548014
             * +"longitude": 20.19119074
             * +"distance": 3.98151975
             * +"speed": 2.97128339
             * +"max_depth": 16.86963033
             * +"depth_values": "{"4.17": {"u": "0.278", "v": "0.077", "w": "0.428", "db": "73.225"}, "6.17": {"u": "0.026", "v": "-0.162", "w": "0.477", "db": "69.007"}, "8.17": {"u": "0.005", "v": "-0.193", "w": "0.489", "db": "74.506"}, "10.17": {"u": "-0.151", "v": "0.003", "w": "0.494", "db": "71.905"}, "12.17": {"u": "-0.013", "v": "-0.302", "w": "0.470", "db": "70.166"}}"
             * +"expedition_id": 1
             * +"appliance_id": 1
             * +"date": "2019-08-07 00:00:00"*/
            $item = (array) $item;

            if ($item['speed'] < $this->storageParams->params->get('speed')) {
                continue;
            }

            if (!$startItem) $startItem = $item;

            foreach ($item['depths'] as $depth => $depth_value) {
                $depth_value = (array)$depth_value;

                $depth = (int)$depth;

                $depths[] = $depth;

                if (!isset($stepAverageArray[$depth])) {
                    $stepAverageArray[$depth] = array();
                }

                # {"u": "0.278", "v": "0.077", "w": "0.428", "db": "73.225"}
                $stepAverageArray[$depth][] = self::getParamAndValues($depth_value);
            }

            if (++$count >= $this->storageParams->params->get('average')) {
                $stepAverageArray = $this->saveAverage($stepAverageArray, $startItem = $item, $rowCount);
                $rowCount++;
                $count = 0;
                $startItem = [];
            }
        }

        if ($count && $this->storageParams->params->get('average')) {
            $this->saveAverage($stepAverageArray, $item, $rowCount);
        }

        $depths = array_values(array_unique($depths));
        $distance = array_values(array_unique(array_column($this->final, 'distance')));
        return ['depth' => $depths, 'distance' => $distance, 'data' => $this->final];
    }

    private function saveAverage(array $saveFrom, array $depthData, int $row) : array {
        foreach ($saveFrom as $depth => $vector) {

            $len = count($vector);
            $array = [
                'u' => 0.0,
                'v' => 0.0,
                'w' => 0.0,
                'db'=> 0.0
            ];
            foreach ($vector as $item) {
                $array['u'] += $item['u'];
                $array['v'] += $item['v'];
                $array['w'] += $item['w'];
                $array['db'] += $item['db'];
            }

            $u = $array['u'] / $len;
            $v = $array['v'] / $len;

            $array = $depthData;
            $this->final[] = array_merge($depthData, [
                'sid'       =>  $this->storageParams->params->get('average') * $row,
                'latitude'  =>  (float)$depthData['latitude'],
                'longitude' =>  (float)$depthData['longitude'],
                'distance' => round($depthData['distance']),
                'speed'     =>  (float)$depthData['speed'],
                'max_depth' =>  (float)$depthData['max_depth'],
                'depth'     =>  (float)$depth,
                'u'         =>  round($u, 3),
                'v'         =>  round($v, 3),
                'w'         =>  round($array['w'] / $len, 3),
                'db'        =>  round($array['db'] / $len, 3),
                'vector'    =>  round($this->getRealVector($u, $v), 3),
                'angle' => round($this->getAngle($u, $v), 3),
            ]);
        }

        return [];
    }

    private function getRealVector( float $u, float $v): float
    {
        return sqrt( ($u ** 2) + ($v ** 2));
    }

    private function getAngle(float $u, float $v) {
        if ($u === 0.0 || $v === 0.0) {
            return 0;
        }

        if ($u > 0.0 && $v > 0.0) {
            return atan($u / $v) / 0.0175;
        }

        if ($u > 0.0 && $v < 0.0) {
            return 90.0 + atan(abs($v) / $u) / 0.0175;
        }

        if ($u < 0.0 && $v < 0.0) {
            return 180.0 + atan(abs($u) / abs($v)) / 0.0175;
        }

        if ($u < 0.0 && $v > 0.0) {
            return 270.0 + atan($v / abs($u)) / 0.0175;
        }

        return 0;
    }

    public static function getParamAndValues( array $params): array
    {
        $result = [];
        foreach ($params as $param => $param_value) {
            $result[$param] = $param_value;
        }

        return $result;
    }
}